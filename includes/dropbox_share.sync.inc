<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Admin ajax request to synchronize deltas and thumbnail information.
 *
 * Optionally specify which Dropbox User to update.
 * Otherwise start with the least recently updated and proceed forward.
 *
 * @param int $dropbox_uid
 *    Optionally update a specific user, instead of the next one that needs it.
 */
function dropbox_share_user_sync_ajax($dropbox_uid = NULL) {
  $output = array();

  // Do not cache this ajax request.
  drupal_page_is_cacheable(FALSE);

  if (variable_get('dropbox_share_delta_user_sync', DROPBOX_SHARE_DELTA_USER_SYNC)) {

    // Cache the entire output of this call for a POLL_INTERVAL.
    $last_called = variable_get('dropbox_share_delta_sync_ajax_called', 0);

    if ($last_called < time() - floor(DROPBOX_SHARE_DELTA_SYNC_POLL / 1000)) {
      variable_set('dropbox_share_delta_sync_ajax_called', time());
      // We can re-run the ajax request now
      // Run the sync request.
      $stats = _dropbox_share_sync(intval($dropbox_uid));

      // Create a human-readable equivalent of the stats for notification.
      $notifications = array();
      foreach (array('delta', 'thumbnail') as $stat_type) {
        if (isset($stats[$stat_type])) {
          foreach ($stats[$stat_type] as $stat => $file_count) {
            if (
              in_array($stat, array('added', 'updated', 'removed'))
              && $file_count
              && is_int($file_count)
            ) {
              $stat_name = 'items';
              if ($stat_type == 'delta') {
                $stat_name = $file_count === 1 ? 'item' : 'items';
              }
              else {
                if ($stat_type == 'thumbnail') {
                  $stat_name = $file_count === 1 ? 'thumbnail' : 'thumbnails';
                }
              }
              $notifications[] = t('@filecount @stat_name @stat', array(
                '@filecount' => $file_count,
                '@stat_name' => $stat_name,
                '@stat' => $stat,
              ));
            }
          }
        }
      }

      $output['stats'] = $stats;
      $output['notifications'] = $notifications;
      variable_set('dropbox_share_delta_sync_ajax_cached', $output);
      variable_set('dropbox_share_delta_sync_ajax_called', time());
    }
    else {
      // Serve from cache.
      $output = variable_get('dropbox_share_delta_sync_ajax_cached', array());
    }
  }

  // Report the stats via json.
  drupal_json_output($output);

  // Since no caching takes place we can skip further all future hooks.
  module_invoke_all('exit');
  exit();
}


/**
 * Update delta and thumbnails.
 *
 * @param int $dropbox_uid
 *    Optionally update only one account.
 * @param string $mode
 *    Optionally specify 'delta' or 'thumbnail' to force a specific update.
 *
 * @return array
 *    Returns statistics on the sync job.
 */
function _dropbox_share_sync($dropbox_uid = NULL, $mode = 'auto') {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.user');
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.delta');
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.thumbnail');

  $stats = array();

  if (
    _dropbox_share_user_count()
    && lock_acquire(__METHOD__, DROPBOX_SHARE_DELTA_SYNC_POLL * 100 / 1000)
  ) {

    $duration_start_time = microtime(TRUE);
    $started = time();

    /* Priority 1 - Known incomplete delta
     *
     * The first priority will be any users that we already know have a delta
     * flag set "has_more", starting with the account that is the most out of
     * date.
     */
    if (
      !$dropbox_uid
      && in_array($mode, array('delta', 'auto'))
      && variable_get('dropbox_share_user_count', 0)
    ) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'dropbox_share_user');
      $query->propertyCondition('dropbox_delta_has_more', 1);
      $query->propertyOrderBy('delta_updated', 'ASC');
      $query->range(0, 1);
      $result = $query->execute();
      if (!empty($result['dropbox_share_user'])) {
        $dropbox_uid = key($result['dropbox_share_user']);
        if ($dropbox_uid) {
          $mode = 'delta';
        }
      }
    }

    /* Priority 2 - Users previously in sync
     *
     * The second priority will be the next user that WAS in sync and has not
     * been checked in the longest amount of time.
     * Only select this user if the account was last checked prior to the
     * configured "rest" period to prevent overloading.
     */
    if (
      !$dropbox_uid
      && in_array($mode, array('delta', 'auto'))
      && variable_get('dropbox_share_user_count', 0)
    ) {
      $rest_duration = intval(variable_get('dropbox_share_delta_rest', DROPBOX_SHARE_DELTA_REST));

      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'dropbox_share_user');
      // This was last updated prior to $rest minutes ago,
      // thus we should check to see if there are delta updates now.
      $query->propertyCondition('delta_updated', $started - $rest_duration, '<');
      $query->propertyOrderBy('delta_updated', 'ASC');
      $query->range(0, 1);
      $result = $query->execute();
      if (!empty($result['dropbox_share_user'])) {
        $dropbox_uid = key($result['dropbox_share_user']);
        if ($dropbox_uid) {
          $mode = 'delta';
        }
      }
    }

    /* Priority 3 - Thumbnails
     *
     * Assuming we are in sync with all deltas, next we can collect thumbnails
     * if configured to do so.
     * Start with user that last had it's thumbnails checked.
     */
    if (
      !$dropbox_uid
      && variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS)
      && in_array($mode, array('thumbnail', 'auto'))
      && variable_get('dropbox_share_user_count', 0)
    ) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'dropbox_share_user');
      // Unlike the delta we will check thumbnails in a round-robin method with
      // user accounts since the thumbnail update method will start with oldest
      // delta entries on it's own.
      $query->propertyOrderBy('thumbnails_updated', 'ASC');
      $query->range(0, 1);
      $result = $query->execute();
      if (!empty($result['dropbox_share_user'])) {
        $dropbox_uid = key($result['dropbox_share_user']);
        if ($dropbox_uid) {
          $mode = 'thumbnail';
        }
      }
    }

    // Load the user node and establish an API client.
    if (
      $dropbox_uid
      && in_array($mode, array('delta', 'thumbnail'))
    ) {
      $dropbox_user = _dropbox_share_user_load($dropbox_uid);
      if ($dropbox_user) {
        module_load_include('inc', 'dropbox_share', 'dropbox_share.api');
        $client = dropbox_share_api_client($dropbox_user);
        if ($client) {
          switch ($mode) {
            case 'delta':

              // Update delta for the user connected by $client.
              $stats['delta'] = _dropbox_share_delta_update($client, $dropbox_user);

              // If any delta updates actually took or more are needed stop.
              // Otherwise we would have spare time to do a thumbnail update.
              if (
                $stats['delta']['added']
                || $stats['delta']['updated']
                || $stats['delta']['removed']
                || $stats['delta']['has_more']
              ) {
                break;
              }
              else {
                $mode = 'all';
              }
            case 'thumbnail':
              // Update thumbnails for the user connected by $client.
              $stats['thumbnail'] = _dropbox_share_thumbnail_sync($client, $dropbox_user);
              break;
          }
        }
      }
    }

    $stats['mode'] = $mode;
    $stats['started'] = $started;
    $stats['duration_ms'] = floor((microtime(TRUE) - $duration_start_time) * 1000);

    // Update user statistics.
    $stats += _dropbox_share_user_update_stats($dropbox_user);

    // Cache the stats for quick retrieval for repeated polls.
    variable_set('dropbox_share_delta_sync_stats', $stats);

    // Indicate that we are done and another synchronization can begin again.
    lock_release(__METHOD__);
  }
  else {
    // If locked, return the last cached result.
    $stats = variable_get('dropbox_share_delta_sync_stats', $stats);
  }

  return $stats;
}
