<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Update/create a thumbnail entity by downloading a fresh copy given parameters.
 *
 * @param \Dropbox\Client $client
 * @param $path
 * @param $format
 * @param $size
 * @param $did
 * @param $entity
 * @return null|array
 *  Returns the generated/saved entity
 */
function _dropbox_share_thumbnail_save(Dropbox\Client $client, $path, $format, $size, $did, $entity = NULL) {

  // New thumbnail.
  if (!isset($entity)) {
    $entity = entity_create('dropbox_share_thumbnail', array());
    $entity->is_new = 1;
    $entity->download_attempts = 0;
  }

  $entity->did = $did;
  $entity->dropbox_size = $size;
  $entity->download_attempts++;

  $metadata = _dropbox_share_thumbnail_download($client, $path, $format, $size, $did);
  if ($metadata) {
    // Now create and save the new entity.
    $entity->uri = $metadata['uri'];
    $entity->dropbox_rev = $metadata['rev'];
  }
  else {
    // Could not download the thumbnail.
    // This usually means the image is in a format that doesn't support thumbnails
    // and we should not try again.
    // A partially null entry will act as a placeholder in the database.
  }
  entity_save('dropbox_share_thumbnail', $entity);

  return $entity;
}

/**
 * Retrieve and save a thumbnail with the Dropbox API
 *
 * @param \Dropbox\Client $client
 * @param $path
 * @param $format
 * @param $size
 * @param $did
 * @return null/array Returns the metadata of the downloaded file, with final path
 * @throws \Dropbox\Exception_BadResponseCode
 * @throws \Dropbox\Exception_OverQuota
 * @throws \Dropbox\Exception_RetryLater
 * @throws \Dropbox\Exception_ServerError
 */
function _dropbox_share_thumbnail_download(Dropbox\Client $client, $path, $format, $size, $did) {
  // Used to prevent establishing the real path of the thumbnail URI  repeatedly.
  $root = &drupal_static(__METHOD__ . '_root', FALSE);
  // Used to prevent checking folder existence repeatedly.
  $folders = &drupal_static(__METHOD__ . '_folders', array());
  $success = $metadata = $response_body = NULL;

  try {
    list($metadata, $response_body) = $client->getThumbnail($path, $format, $size);
  }
  catch (Dropbox\Exception_NetworkIO $ex) {
    watchdog('dropbox', "Network issue", array($ex->getMessage()), WATCHDOG_ERROR);
  }
  catch (Exception $ex) {
    watchdog('dropbox', "Error retrieving thumbnail", array($ex->getMessage()), WATCHDOG_WARNING);
  };

  if ($metadata && $response_body) {

    // Discern the root folder for thumbnails.
    if (!$root) {
      $root_uri = file_stream_wrapper_get_instance_by_uri(DROPBOX_SHARE_THUMBNAIL_URI);
      $root = $root_uri->realpath();
    }

    // Split thumbnails into folders of hundreds
    // There will be no more than 100 files/folders per level
    // example  = 34        9384        4289734           101       2
    $padded = str_pad((string) $did, 3, '0', STR_PAD_LEFT); // $padded  = 034       9384        4289734           101       002
    $name = substr($padded, -2);                            // $name    = 34        84          34                01        02
    $front = substr($padded, 0, strlen($padded) - 2);       // $front   = 0         93          42897             1         0
    $folder = implode('/', str_split($front, 1));           // $folder  = 0         9/3         4/2/8/9/7         1         0
    // result   = 0/34.jpeg 9/3/84.jpeg 4/2/8/9/7/34.jpeg 1/01.jpeg 0/02.jpg
    // Ensure this folder exists.
    if (
      !isset($folders[$root . '/' . $folder])
      && !is_dir($root . '/' . $folder)
    ) {
      drupal_mkdir($root . '/' . $folder, NULL, TRUE);
    }
    // Set a flag for quick lookup.
    $folders[$root . '/' . $folder] = TRUE;

    // Write the thumbnail to disk.
    $thumbnail_path = $root . '/' . $folder . '/' . $name . '.' . $format;
    $fh = fopen($thumbnail_path, 'w');
    if ($fh) {
      fwrite($fh, $response_body);
      if ($fh !== FALSE) {
        $metadata['uri'] = DROPBOX_SHARE_THUMBNAIL_URI . '/' . $folder . '/' . $name . '.' . $format;
        $success = fclose($fh);
      }
    }
  }

  return $success ? $metadata : NULL;
}

/**
 * Run through delta entries, and update entries with useful metadata where
 * it applies. This follows the internal thumbnail rate limit.
 *
 * @param \Dropbox\Client $client
 * @param $dropbox_user
 * @return array
 */
function _dropbox_share_thumbnail_sync(Dropbox\Client $client, $dropbox_user) {
  $limit = &drupal_static(__METHOD__, intval(variable_get('dropbox_share_thumbnail_rate', DROPBOX_SHARE_THUMBNAIL_RATE)));
  $size = variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS);

  $stats = array(
    'added' => 0,
    'updated' => 0,
    'removed' => 0,
  );

  $stats['dropbox_uid'] = $dropbox_user->dropbox_uid;

  // So long as we are below our rate limit, we can loop to accomplish more.
  do {
    $initial_limit = $limit;

    // Update the user to indicate the last time we checked to update thumbnails
    // Keeps the users in an appropriate queue.
    _dropbox_share_user_save(array(
      'thumbnails_updated' => time()
    ), $dropbox_user);

    /*
     * Priority 1 - New Files
     *
     * Handle new delta entries, thus thumbnails need to be downloaded
     * And new dropbox_share_thumbnail entities created.
     * Start with the oldest delta entries and stop when we hit our limit
     * so that we maintain a "first come, first serve" mentality.
     * This will consequentially start with the root folder, and recursively
     * dive deeper upon each run.
     */
    if ($limit > 0) {
      $sql = '
        SELECT
          d.id as did,
          d.dropbox_path,
          d.dropbox_mime_type
        FROM
          {dropbox_share_delta} d
        LEFT JOIN
          {dropbox_share_thumbnail} t
            ON t.did = d.id
        WHERE
          d.dropbox_thumb_exists = 1            /* Thumbnail exists           */
          AND t.id IS NULL                      /* Thumbnail entity needed    */
        ORDER BY
          d.updated ASC                         /* Start with oldest delta    */
      ';
      $result = db_query_range($sql, 0, floor($limit));
      if ($result) {
        // Import the thumbnails.
        foreach ($result as $row) {
          $path = $row->dropbox_path;
          // Dropbox supports png and jpeg format for thumbnails, so we will
          // select the format that best fits the source image's mime type.
          switch ($row->dropbox_mime_type) {
            case 'image/png':
            case 'image/gif':
              $format = 'png';
              break;

            default:
              $format = 'jpeg';
          }
          // The delta ID will be used to name the thumbnail file itself.
          $entity = _dropbox_share_thumbnail_save($client, $path, $format, $size, $row->did);
          if (isset($entity->dropbox_rev)) {
            $stats['added']++;
          }
          $limit--;
        }
      }
    }

    /*
     * Priority 2 - File changes
     *
     * Handle situations where the file has been altered since the thumbnail was
     * downloaded, or our settings have changed such that we desire a different
     * resolution of thumbnail.
     */
    if ($limit > 0) {
      $sql = '
        SELECT
          d.id as did,
          d.dropbox_path,
          d.dropbox_mime_type,
          t.id as tid
        FROM
          {dropbox_share_delta} d
        LEFT JOIN
          {dropbox_share_thumbnail} t
            ON t.did = d.id
        WHERE
          d.dropbox_thumb_exists = 1            /* Thumbnail and delta exists */
          AND NOT t.dropbox_rev IS NULL         /* Not a corrupted thumbnail  */
          AND (NOT t.dropbox_rev = d.dropbox_rev/* Changed revision           */
            OR NOT t.dropbox_size = :size)      /* Changed size setting       */
        ORDER BY
          d.updated ASC                         /* Start with oldest delta    */
      ';
      $result = db_query_range($sql, 0, floor($limit), array(':size' => $size));
      if ($result) {
        // Load the entities up front for performance.
        $tids = array();
        foreach ($result as $row) {
          $tids[] = $row->tid;
        }
        if (count($tids)) {
          $entities = entity_load('dropbox_share_thumbnail', $tids);

          // Import the thumbnails.
          foreach ($result as $row) {
            $path = $row->dropbox_path;
            // Dropbox supports png and jpeg format for thumbnails, so we will
            // select the format that best fits the source image's mime type
            switch ($row->dropbox_mime_type) {
              case 'image/png':
              case 'image/gif':
                $format = 'png';
                break;
              default:
                $format = 'jpeg';
            }
            // The delta ID will be used to name the thumbnail file itself
            if (isset($entities[$row->tid])) {
              $entity = _dropbox_share_thumbnail_save($client, $path, $format, $size, $row->did, $entities[$row->tid]);
              if (isset($entity->dropbox_rev)) {
                $stats['updated']++;
              }
              $limit--;
            }
          }
        }
      }
    }

    /**
     * Priority 3 - Deleted files
     *
     * Handle removed delta items, thus thumbnails to be removed
     * Deletions are very lightweight (no api call required), so we can
     * safely multiply the speed here to help expunge them faster.
     */
    if ($limit > 0) {
      $deletion_limit_multiplier = 10;
      $sql = '
        SELECT
          t.id as tid,
          t.uri
        FROM
          {dropbox_share_thumbnail} t
        LEFT JOIN
          {dropbox_share_delta} d
            ON t.did = d.id
        WHERE
          d.id IS NULL                          /* Delta no longer exists     */
        ORDER BY
          t.id ASC                              /* Start with oldest thumbs   */
      ';
      $result = db_query_range($sql, 0, floor($limit * $deletion_limit_multiplier));
      if ($result) {
        // Remove the files
        $tids = array();
        foreach ($result as $row) {
          $real_path = drupal_realpath($row->uri);
          drupal_unlink($real_path);
          $tids[] = $row->tid;
          $limit -= (1 / $deletion_limit_multiplier);
          $stats['removed']++;
        }
        // Bulk delete the db entries now that files have been deleted
        if (count($tids)) {
          db_delete('dropbox_share_thumbnail')
            ->condition('id', $tids, 'IN')
            ->execute();
        }
      }
    }

    /**
     * Priority 4 - Retry download attempts
     *
     * For various reasons a thumbnail download can fail.
     * We will retry them, but limit the number of attempts.
     */
    if ($limit > 0) {
      $sql = '
        SELECT
          d.id as did,
          d.dropbox_path,
          d.dropbox_mime_type,
          t.id as tid
        FROM
         dropbox_share_delta d
        LEFT JOIN
          dropbox_share_thumbnail t
            ON t.did = d.id
        WHERE
          d.dropbox_thumb_exists = 1            /* Thumbnail and delta exists */
          AND t.dropbox_rev IS NOT NULL         /* Thumbnail not downloaded   */
          AND t.download_attempts < :attempts   /* DL limit not yet exceeded  */
        ORDER BY
          d.download_attempts ASC,              /* Start with fewest attempts */
          t.id ASC                              /* and then oldest thumbs     */
      ';
      $result = db_query_range($sql, 0, floor($limit), array(
        ':attempts' => DROPBOX_SHARE_THUMBNAIL_ATTEMPTS
      ));
      if ($result) {
        // Load the entities up front for performance
        $tids = array();
        foreach ($result as $row) {
          $tids[] = $row->tid;
        }
        if (count($tids)) {
          $entities = _dropbox_share_thumbnail_load_multiple($tids);

          // Import the thumbnails
          foreach ($result as $row) {
            $path = $row->dropbox_path;
            // Dropbox supports png and jpeg format for thumbnails, so we will
            // select the format that best fits the source image's mime type
            switch ($row->dropbox_mime_type) {
              case 'image/png':
              case 'image/gif':
                $format = 'png';
                break;
              default:
                $format = 'jpeg';
            }
            // The delta ID will be used to name the thumbnail file itself
            if (isset($entities[$row->tid])) {
              $entity = _dropbox_share_thumbnail_save($client, $path, $format, $size, $row->did, $entities[$row->tid]);
              if (isset($entity->dropbox_rev)) {
                $stats['added']++;
              }
              $limit--;
            }
          }
        }
      }
    }

    // If some work was done, but we have not hit the limit, continue
    // Don't loop forever if there is no work being done
  } while ($limit > 0 && $initial_limit > $limit);

  /**
   * Flush the folder cache for this account since the contents have changed
   */
  if ($stats['added'] || $stats['updated'] || $stats['removed']) {
    module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.folder');
    _dropbox_share_folder_cache_clear($dropbox_user->dropbox_uid);
  }

  return $stats;
}

/**
 * Load a single thumbnail for a delta item.
 *
 * @param $tid
 * @param bool $reset
 * @return mixed|null
 */
function _dropbox_share_thumbnail_load($tid, $reset = FALSE) {
  $entities = _dropbox_share_thumbnail_load_multiple(array($tid), array(), $reset);
  return $entities ? reset($entities) : NULL;
}

/**
 * Load multiple thumbnails for delta items.
 *
 * @param array $tids
 * @param array $conditions
 * @param bool $reset
 * @return mixed
 */
function _dropbox_share_thumbnail_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('dropbox_share_thumbnail', $tids, $conditions, $reset);
}

/**
 * Load multiple thumbnails by delta ids.
 *
 * @param array $dids
 *    The delta IDs that the thumbnails would correspond to
 * @param bool $only_valid
 *    Only include thumbnails that have been successfully downloaded.
 * @return mixed
 */
function _dropbox_share_thumbnail_load_multiple_by_delta($dids = array(), $only_valid = TRUE) {
  $entities = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'dropbox_share_thumbnail');
  $query->propertyCondition('did', $dids, 'IN');
  if ($only_valid) {
    $query->propertyCondition('uri', 'NULL', '!=');
  }
  $result = $query->execute();
  if (!empty($result['dropbox_share_thumbnail'])) {
    $tids = array_keys($result['dropbox_share_thumbnail']);
    $entities += _dropbox_share_thumbnail_load_multiple($tids);
  }

  return $entities;
}

