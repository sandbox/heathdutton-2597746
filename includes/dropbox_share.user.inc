<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Given a Dropbox client, create/update a user record for them in the db.
 *
 * @param \Dropbox\Client $client
 * @return Entity|null
 * @throws \Dropbox\Exception_BadResponseCode
 * @throws \Dropbox\Exception_OverQuota
 * @throws \Dropbox\Exception_RetryLater
 * @throws \Dropbox\Exception_ServerError
 */
function _dropbox_share_user_update(Dropbox\Client $client) {
  $dropbox_user = NULL;
  $dropbox_account_info = $client->getAccountInfo();
  if ($dropbox_account_info) {
    $dropbox_user_params = array();
    $dropbox_user_params['dropbox_uid'] = $dropbox_account_info['uid'];
    $dropbox_user_params['dropbox_display_name'] = $dropbox_account_info['display_name'];
    $dropbox_user_params['dropbox_email'] = $dropbox_account_info['email'];
    $dropbox_user_params['dropbox_token'] = base64_encode(encrypt($client->getAccessToken()));
    $dropbox_user_params['dropbox_key'] = base64_encode(encrypt($_SESSION['dropbox_share_key']));
    $dropbox_user_params['dropbox_secret'] = base64_encode(encrypt($_SESSION['dropbox_share_secret']));
    if (!empty($dropbox_account_info['team'])) {
      $dropbox_user_params['dropbox_team_id'] = $dropbox_account_info['team']['team_id'];
      $dropbox_user_params['dropbox_team_name'] = $dropbox_account_info['team']['name'];
    }
    if (!empty($dropbox_account_info['quota_info'])) {
      $dropbox_user_params['dropbox_quota_info_normal'] = $dropbox_account_info['quota_info']['normal'];
      $dropbox_user_params['dropbox_quota_info_shared'] = $dropbox_account_info['quota_info']['shared'];
      $dropbox_user_params['dropbox_quota_info_quota'] = $dropbox_account_info['quota_info']['quota'];
    }
    $dropbox_user = _dropbox_share_user_save($dropbox_user_params);
  }
  return $dropbox_user;
}

/**
 * Get the current number of enabled Dropbox user accounts.
 *
 * @param bool $update
 * @return int|mixed\
 */
function _dropbox_share_user_count($update = FALSE) {
  $count = 0;
  if ($update) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'dropbox_share_user');
    $query->propertyCondition('status', '1');
    $query->count();
    $count = intval($query->execute());
    variable_set('dropbox_share_user_count', $count);
  }
  else {
    $count = variable_get('dropbox_share_user_count', $count);
  }
  return $count;
}

/**
 * Dropbox user options for field selection
 * Only include active users
 *
 * @return array
 */
function _dropbox_share_user_options() {
  $options = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'dropbox_share_user');
  $query->propertyCondition('status', '1');
  $result = $query->execute();
  if (!empty($result['dropbox_share_user'])) {
    $uids = array_keys($result['dropbox_share_user']);
    $user_entities = _dropbox_share_user_load_multiple($uids);
    foreach ($user_entities as $user_entity) {
      $uid = $user_entity->dropbox_uid;
      $team = $user_entity->dropbox_team_name;
      $team = $team ? $team : t('Single User');
      $name = $user_entity->dropbox_display_name;
      $name = $name ? $name : $uid;
      $options[$uid] = $team . ' - ' . $name . ' - ' . $user_entity->dropbox_email;
    }
    natsort($options);
  }

  // Force selection only if there is more than one option.
  if (count($options) > 1) {
    $options = array(0 => t('- Select -')) + $options;
  }

  return $options;
}

/**
 * Load a single user.
 *
 * @param  int $dropbox_uid
 * @param  bool $reset
 * @return Entity/null
 */
function _dropbox_share_user_load($dropbox_uid, $reset = FALSE) {
  $entities = _dropbox_share_user_load_multiple(array($dropbox_uid), array(), $reset);
  return $entities ? reset($entities) : NULL;
}

/**
 * Load multiple users.
 *
 * @param  array $dropbox_uids
 * @param  array $conditions
 * @param  bool $reset
 * @return array
 */
function _dropbox_share_user_load_multiple($dropbox_uids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('dropbox_share_user', $dropbox_uids, $conditions, $reset);
}

/**
 * Create/Update a Dropbox user record.
 *
 * @param  array $parameters Parameters to save to the entity
 * @param  Entity $entity Optionally save existing entity
 * @return Entity/null
 */
function _dropbox_share_user_save($parameters, Entity &$entity = NULL) {
  global $user;

  // See if this user already exists
  if (!$entity && !empty($parameters['dropbox_uid'])) {
    $entity = _dropbox_share_user_load($parameters['dropbox_uid']);
  }

  // Create a new entity if one does not already exist or was not provided.
  if (!$entity) {
    $entity = entity_create('dropbox_share_user', array());
    $entity->is_new = TRUE;
    $entity->created = time();
    $entity->status = 1;
    $entity->uid = $user->uid;
  }

  foreach ($parameters as $parameter => $value) {
    $entity->{$parameter} = $value;
  }

  $entity->updated = time();

  return entity_save('dropbox_share_user', $entity) ? $entity : NULL;
}

/**
 * Calculate the level of synchronization for an account.
 *
 * @param $dropbox_user
 * @param bool $save_stats
 * @return array
 */
function _dropbox_share_user_update_stats($dropbox_user, $save_stats = TRUE) {
  $stats = array();

  $thumbs_enabled = variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS);

  // By default we assume no delta has yet been synced. We must be at 0%.
  $stats['thumbnail_percentage'] = 0;

  if ($dropbox_user->dropbox_cursor) {

    // Compare thumbnail count vs those that need to be downloaded.
    if ($thumbs_enabled) {
      $sql = '
        SELECT
          COUNT(d.id) as count                  /* Only get the count         */
        FROM
          {dropbox_share_delta} d
        LEFT JOIN
          {dropbox_share_thumbnail} t
            ON t.did = d.id
        WHERE
          d.dropbox_uid = :dropbox_uid          /* The current user ID only   */
          AND d.dropbox_thumb_exists = 1        /* Thumbnail exists remotely  */
          AND t.id IS NOT NULL                  /* Thumbnail exists locally   */
      ';
      $local_total = intval(
        db_query($sql,
          array(':dropbox_uid' => $dropbox_user->dropbox_uid))
          ->fetchField()
      );
      if ($local_total) {
        $sql = '
          SELECT
            COUNT(d.id) as count                /* Only get the count         */
          FROM
            {dropbox_share_delta} d
          LEFT JOIN
            {dropbox_share_thumbnail} t
              ON t.did = d.id
          WHERE
            d.dropbox_uid = :dropbox_uid        /* The current user ID only   */
            AND d.dropbox_thumb_exists = 1      /* Thumbnail exists remotely  */
            AND t.id IS NULL                    /* Thumbnail needed locally   */
        ';
        $remote_total = intval(
          db_query($sql,
            array(':dropbox_uid' => $dropbox_user->dropbox_uid))
            ->fetchField()
        );
        if ($remote_total) {
          // This is the percentage of *known* thumbnails that need to be
          // downloaded. It may not be a complete count if the delta has
          // more items to retrieve.
          $stats['thumbnail_percentage'] = floor(100 / $remote_total * $local_total);
          if ($stats['thumbnail_percentage'] > 100) {
            $stats['thumbnail_percentage'] = 100;
          }
        }
        else {
          // Delta deletions/sync in progress, known state is now at 100%.
          $stats['thumbnail_percentage'] = 100;
        }
      }
    }
  }

  if ($stats && $save_stats) {
    _dropbox_share_user_save($stats, $dropbox_user);
  }
  return $stats;
}
