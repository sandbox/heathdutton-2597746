<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * URL clicked to download a file
 * Discern the media path and redirect to the file immediately.
 *
 * @param $dropbox_uid
 * @param int $did
 * @param int $mode
 */
function dropbox_share_user_media_ajax($dropbox_uid, $did = 0, $mode = 'json') {
  $media = array();

  // Do not cache this ajax request.
  drupal_page_is_cacheable(FALSE);

  // @todo - Include session-based authentication here to ensure that the current user (by ajax) has access to the folder specified by did.

  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.delta');
  $delta_entity = _dropbox_share_delta_load_balanced($did, $dropbox_uid);

  $media = _dropbox_share_media($delta_entity, TRUE);

  switch ($mode) {
    case 'download':
      // Force a redirection and download.
      if (isset($media['url'])) {
        drupal_goto($media['url'] . '?dl=1', array('absolute' => TRUE), 301);
      }
      else {
        drupal_access_denied();
      }
      break;

    case 'preview':
      // Redirect to the file preview.
      if (isset($media['url'])) {
        drupal_goto($media['url'], array('absolute' => TRUE), 301);
      }
      else {
        drupal_access_denied();
      }
      break;

    case 'json':
    default:
      // Report the entries via json.
      drupal_json_output($media);
  }

  // Since no caching takes place we can skip further all future hooks.
  module_invoke_all('exit');
  exit();
}


/**
 * Given a Client and a path, retrieve a living direct URL to the file.
 *
 * @param $delta_entity
 * @param bool $allow_folders
 * @return array
 * @throws \Dropbox\Exception_BadResponseCode
 * @throws \Dropbox\Exception_OverQuota
 * @throws \Dropbox\Exception_RetryLater
 * @throws \Dropbox\Exception_ServerError
 */
function _dropbox_share_media($delta_entity, $allow_folders = FALSE) {
  $media = &drupal_static(__METHOD__);
  $result = array();

  $cache_key = $delta_entity->dropbox_uid . '/' . $delta_entity->id;

  if (isset($media[$cache_key])) {
    $result = $media[$cache_key];
  }
  elseif (
    $cache = cache_get($cache_key, 'dropbox_share_media_cache')
    && isset($cache)
    && time() < $cache->expire
  ) {
    $result = $cache->data;
    $media[$cache_key] = $cache->data;
  }
  elseif (
    $delta_entity
    && (!$delta_entity->dropbox_is_dir || $allow_folders)
  ) {

    // Load the user entity.
    module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.user');
    $dropbox_user = _dropbox_share_user_load($delta_entity->dropbox_uid);

    // Load the api client.
    if ($dropbox_user) {
      module_load_include('inc', 'dropbox_share', 'dropbox_share.api');
      $client = dropbox_share_api_client($dropbox_user);

      // Make the API call to retrieve the download item/folder.
      if ($client) {

        if ($delta_entity->dropbox_is_dir && $allow_folders) {
          // Get a share link for a folder.
          // https://www.dropbox.com/sh/3jrmaj0w5juhwy0/AAB4JaMSyMtzS1oj0QFsBmtFa?dl=0
          $url = $client->createShareableLink($delta_entity->dropbox_path);
          $result['url'] = str_replace('?dl=0', '', $url);
          $result['icon'] = 'folder';
          $result['bytes'] = '0';
          $result['mime_type'] = 'inode/directory';
          // Expire the folder in one hour or so.
          $expire_timestamp = time() + DROPBOX_SHARE_MEDIA_FOLDER_LS;
        }
        else {
          // Get details to download a single file.
          list($url, $expire_datetime) = $client->createTemporaryDirectLink($delta_entity->dropbox_path);
          if ($url && $expire_datetime) {
            $result['icon'] = $delta_entity->dropbox_icon;
            $result['bytes'] = $delta_entity->dropbox_bytes;
            $result['mime_type'] = $delta_entity->dropbox_mime_type;
            $result['url'] = $url;
            // We shave some time off of the expiration to leave room for external caching and load times.
            $expire_timestamp = $expire_datetime->getTimestamp() - DROPBOX_SHARE_MEDIA_LIMIT;
          }
        }
        $file_segments = explode('/', $delta_entity->dropbox_path);
        $result['file_name'] = end($file_segments);
        $result['expire'] = $expire_timestamp;
        $media[$cache_key] = $result;
        cache_set($cache_key, $result, 'dropbox_share_media_cache', $expire_timestamp);
      }
    }
  }
  return $result;
}

