<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Generate the URI that our OAuth request will eventually redirect to.
 *
 * @param bool $custom
 * @param bool $force_https
 * @return mixed|string
 */
function _dropbox_share_auth_redirect_uri($custom = FALSE, $force_https = TRUE) {

  if ($custom || intval(variable_get('dropbox_share_app', DROPBOX_SHARE_APP_DEFAULT)) == DROPBOX_SHARE_APP_CUSTOM) {
    // Custom app behavior.
    $redirect_uri = url('admin/config/services/dropbox-share/redirect', array('absolute' => TRUE));
  }
  else {
    // Default app behavior. We can't send directly to Drupal,
    // since the url is not in the app configuration.
    // so instead we use intermediary redirection.
    $redirect_uri = DROPBOX_SHARE_DEFAULT_OATH_REDIRECT;
  }

  if ($force_https) {
    // SSL Must be enforced for Dropbox OAuth 2.
    $redirect_uri = str_replace('http://', 'https://', $redirect_uri);
  }

  return $redirect_uri;
}

/**
 * Get the dropbox webAuth object.
 *
 * @throws \Exception
 * @throws \OAuthException
 */
function _dropbox_share_auth_start() {
  module_load_include('inc', 'dropbox_share', 'dropbox_share.api');

  $app_info = dropbox_share_api_load();
  if ($app_info) {
    $redirect_uri = _dropbox_share_auth_redirect_uri();
    $token_store = new Dropbox\ArrayEntryStore($_SESSION, 'dropbox_share_csrf_token');
    return new Dropbox\WebAuth($app_info, DROPBOX_SHARE_APP_ID, $redirect_uri, $token_store);
  }
}

/**
 * Complete an OAuth request after being redirected back to Drupal.
 *
 * @return object
 */
function _dropbox_share_web_auth_complete() {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.user');

  $dropbox_user = $dropbox_token = $dropbox_uid = $url_state = NULL;

  try {
    list($dropbox_token, $dropbox_uid, $url_state) = _dropbox_share_auth_start()->finish($_GET);
  }
  catch (Dropbox\WebAuthException_BadRequest $ex) {
    watchdog('dropbox', "dropbox-auth-finish: bad request", array($ex->getMessage()), WATCHDOG_ERROR);
    // Respond with an HTTP 400 and display error page...
  }
  catch (Dropbox\WebAuthException_BadState $ex) {
    // Auth session expired. This will happen if the user takes too long.
    // Restart the auth process, no message or limiting should apply here.
  }
  catch (Dropbox\WebAuthException_Csrf $ex) {
    // Potential attack or session issue.
    watchdog('dropbox', "dropbox-auth-finish: CSRF mismatch", array($ex->getMessage()), WATCHDOG_ERROR);
  }
  catch (Dropbox\WebAuthException_NotApproved $ex) {
    // The user hit "Cancel" and backed out of the OAuth session.
    watchdog('dropbox', "dropbox-auth-finish: not approved", array($ex->getMessage()), WATCHDOG_NOTICE);
  }
  catch (Dropbox\WebAuthException_Provider $ex) {
    watchdog('dropbox', "dropbox-auth-finish: error redirect from Dropbox", array($ex->getMessage()), WATCHDOG_WARNING);
  }
  catch (Dropbox\Exception $ex) {
    watchdog('dropbox', "dropbox-auth-finish: error communicating with Dropbox API", array($ex->getMessage()), WATCHDOG_ERROR);
  }

  // If all went well, store the new dropbox user and token.
  if (isset($dropbox_token) && isset($dropbox_uid) && !isset($ex)) {

    $client = new Dropbox\Client($dropbox_token, DROPBOX_SHARE_APP_ID);
    // Grab user data so that accounts can be identified in the admin later.
    if ($client) {
      $dropbox_user = _dropbox_share_user_update($client);
      if ($dropbox_user) {
        _dropbox_share_user_count(TRUE);
      }
    }
  }

  return $dropbox_user;
}
