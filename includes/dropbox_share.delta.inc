<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Given a Dropbox client, create/update the user's delta in the db
 * This "delta" is just a big file/folder list.
 * It can be updated iteratively much like a git repository.
 *
 * @param \Dropbox\Client $client
 * @param Entity $dropbox_user
 * @return array
 * @throws \Dropbox\Exception_BadResponseCode
 * @throws \Dropbox\Exception_OverQuota
 * @throws \Dropbox\Exception_RetryLater
 * @throws \Dropbox\Exception_ServerError
 */
function _dropbox_share_delta_update(Dropbox\Client $client, Entity $dropbox_user = NULL) {

  $stats = array(
    'added' => 0,
    'updated' => 0,
    'removed' => 0,
    'has_more' => FALSE,
  );

  // Do not update delta if delta storage is disabled.
  if (variable_get('dropbox_share_delta_storage', DROPBOX_SHARE_DELTA_STORAGE)) {
    if (!$dropbox_user) {
      $dropbox_account_info = $client->getAccountInfo();
      if ($dropbox_account_info) {
        $dropbox_user = _dropbox_share_user_load($dropbox_account_info['dropbox_uid']);
      }
    }

    if ($dropbox_user) {

      /*
       * Retrieve the latest delta from the API for this user
       */
      $dropbox_cursor = NULL;
      if (!empty($dropbox_user->dropbox_cursor)) {
        $dropbox_cursor = $dropbox_user->dropbox_cursor;
      }

      // Note: We never send a path_prefix because we always want the entire tree.
      $delta = $client->getDelta($dropbox_cursor);

      if ($delta) {
        // Clear out all old deltas if we are told to reset.
        if ($delta['reset']) {
          // Dropbox has informed us that our delta is vastly out of date (or new).
          // So we must flush out the delta to pull fresh for speed.
          $stats['removed'] += db_delete('dropbox_share_delta')
            ->condition('dropbox_uid', $dropbox_user->dropbox_uid)
            ->execute();
        }

        // Sort delta entries from the API by path as our internal key.
        $delta_entries = array();
        if (!empty($delta['entries'])) {
          foreach ($delta['entries'] as $delta_entry) {
            $delta_entries[$delta_entry[0]] = $delta_entry[1];
          }
        }

        // Delete pre-existing delta entries that have been deleted remotely.
        if (!$delta['reset']) {
          $paths_deleted = array();
          foreach ($delta_entries as $path => $delta_entry) {
            if ($delta_entry == NULL) {
              $paths_deleted[] = $path;
              unset($delta_entries[$path]);
            }
          }
          if (count($paths_deleted)) {
            db_delete('dropbox_share_delta')
              ->condition('dropbox_uid', $dropbox_user->dropbox_uid)
              ->condition('dropbox_path', $paths_deleted, 'IN')
              ->execute();
            $stats['removed'] += count($paths_deleted);

            // @todo - For folders, delete all child entries. The API is not that explicit on deletions. Will require an extra query.
          }
        }

        // Update pre-existing delta entries if we did not reset.
        if (!$delta['reset'] && count($delta_entries)) {

          // Query to get the list of entities that need updating in place.
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'dropbox_share_delta');
          $query->propertyCondition('dropbox_uid', $dropbox_user->dropbox_uid);
          $query->propertyCondition('dropbox_path', array_keys($delta_entries), 'IN');
          $result = $query->execute();
          if (!empty($result['dropbox_share_delta'])) {
            $entities = _dropbox_share_delta_load_multiple(array_keys($result['dropbox_share_delta']));
            foreach ($entities as $entity) {
              if (isset($delta_entries[$entity->dropbox_path])) {
                _dropbox_share_delta_update_entity($delta_entries[$entity->dropbox_path], $dropbox_user->dropbox_uid, $entity);
                unset($delta_entries[$entity->dropbox_path]);
                $stats['updated']++;
              }
            }
          }
        }

        // Insert new delta entries, if any remain.
        if (count($delta_entries)) {
          foreach ($delta_entries as $path => $delta_entry) {
            _dropbox_share_delta_update_entity($delta_entry, $dropbox_user->dropbox_uid);
            unset($delta_entries[$path]);
            $stats['added']++;
          }
          _dropbox_share_delta_bulk_entity_insert();
        }

        /*
         * Update the delta cursor for this user sp that future delta
         * updates will only need to retrieve the difference
         */
        _dropbox_share_user_save(array(
          'delta_updated' => time(),
          'dropbox_cursor' => $delta['cursor'],
          'dropbox_delta_has_more' => $delta['has_more'] ? '1' : '0',
        ), $dropbox_user);

        // Flush the folder cache for this account since the contents have changed.
        if ($stats['added'] || $stats['updated'] || $stats['removed']) {
          module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.folder');
          _dropbox_share_folder_cache_clear($dropbox_user->dropbox_uid);
        }

        $stats['has_more'] = $delta['has_more'];
      }
      $stats['dropbox_uid'] = $dropbox_user->dropbox_uid;
    }
  }

  // This function can be run again to continue the update where it left off.
  return $stats;
}

/**
 * Given a delta entry, update or create the entity equivalent and save changes
 * only if needed.
 *
 * @param array $delta_entry
 * @param null $dropbox_uid
 * @param \Entity $entity
 * @return bool|\Entity
 */
function _dropbox_share_delta_update_entity(Array $delta_entry, $dropbox_uid = NULL, Entity $entity = NULL) {
  $bulk_insert_queue = &drupal_static('dropbox_share_delta_entity_bulk_insert', array());

  if (!$entity) {
    $entity = entity_create('dropbox_share_delta', array());
    $entity->is_new = TRUE;
    $entity->dropbox_uid = $dropbox_uid;
  }

  // Map API keys to Entity keys by property type.
  $key_map = array(
    // Converted from bool value to '1' or '0' for int storage by Drupal standards.
    'boolean' => array(
      'thumb_exists' => 'dropbox_thumb_exists',
      'is_dir' => 'dropbox_is_dir',
      'read_only' => 'dropbox_read_only',
    ),
    // Stored as provided as integers.
    'integer' => array(
      'bytes' => 'dropbox_bytes',
      'revision' => 'dropbox_revision',
    ),
    // Stored as provided as strings.
    'string' => array(
      'rev' => 'dropbox_rev',
      'path' => 'dropbox_path',
      'icon' => 'dropbox_icon',
      'root' => 'dropbox_root',
      'mime_type' => 'dropbox_mime_type',
      'parent_shared_folder_id' => 'dropbox_parent_shared_folder_id',
    ),
    // These are converted from strings to unix timestamp integers to match Drupal standards.
    'unix' => array(
      'client_mtime' => 'dropbox_client_mtime',
      'modified' => 'dropbox_modified',
    ),
  );

  // Handle booleans.
  foreach ($key_map['boolean'] as $api_key => $entity_key) {
    if (isset($delta_entry[$api_key])) {
      // We use string boolean equivalents for entity update comparison.
      $entity->{$entity_key} = $delta_entry[$api_key] ? '1' : '0';
    }
  }

  // Handle integers.
  foreach ($key_map['integer'] as $api_key => $entity_key) {
    if (isset($delta_entry[$api_key])) {
      // We convert these to strings for entity update comparison.
      $entity->{$entity_key} = (string) $delta_entry[$api_key];
    }
  }

  // Handle strings.
  foreach ($key_map['string'] as $api_key => $entity_key) {
    if (isset($delta_entry[$api_key])) {
      $entity->{$entity_key} = $delta_entry[$api_key];
    }
  }

  // Handle unix timestamp conversions.
  foreach ($key_map['unix'] as $api_key => $entity_key) {
    if (isset($delta_entry[$api_key])) {
      $entity->{$entity_key} = (string) strtotime($delta_entry[$api_key]);
    }
  }

  // Handle nested ids that we desire.
  if (!empty($delta_entry['shared_folder']['shared_folder_id'])) {
    $entity->dropbox_shared_folder_id = (string) $delta_entry['shared_folder']['shared_folder_id'];
  }
  if (!empty($delta_entry['modifier']['uid'])) {
    $entity->dropbox_modifier_uid = (string) $delta_entry['modifier']['uid'];
  }

  // Only save if this is a new entry, or an updated one with actual changes.
  $entity->updated = time();
  if (!$entity->is_new) {
    // Do not create entities one at a time, and instead bulk-insert.
    entity_save('dropbox_share_delta', $entity);
  }
  else {
    // Instead of running thousands of entity_saves, preserve them to be inserted in bulk.
    $bulk_insert_queue[] = $entity;
  }

  return $entity;
}

/**
 * Bulk insert new delta entries
 * This is roughly 20% faster than running entity_save 2000 times.
 *
 * @throws \Exception
 */
function _dropbox_share_delta_bulk_entity_insert() {
  $entities = &drupal_static('dropbox_share_delta_entity_bulk_insert', array());
  $result = NULL;

  if (count($entities)) {

    // Map for entity properties to be inserted.
    $properties = array(
      'dropbox_uid',
      'dropbox_modifier_uid',
      'dropbox_rev',
      'dropbox_revision',
      'dropbox_thumb_exists',
      'dropbox_path',
      'dropbox_is_dir',
      'dropbox_icon',
      'dropbox_shared_folder_id',
      'dropbox_parent_shared_folder_id',
      'dropbox_read_only',
      'dropbox_bytes',
      'dropbox_client_mtime',
      'dropbox_modified',
      'dropbox_root',
      'dropbox_mime_type',
      'updated',
    );

    $query = db_insert('dropbox_share_delta')
      ->fields($properties);

    foreach ($entities as $entity) {
      $values = array();
      foreach ($properties as $property) {
        if (isset($entity->{$property})) {
          $values[$property] = $entity->{$property};
        }
        else {
          $values[$property] = NULL;
        }
      }
      $query->values($values);
    }

    $result = $query->execute();
    $entities = array();
  }

  return $result;
}

/**
 * Load a single delta entry.
 *
 * @param int $did
 * @param bool $reset
 * @return mixed|null
 */
function _dropbox_share_delta_load($did, $reset = FALSE) {
  $entities = _dropbox_share_delta_load_multiple(array($did), array(), $reset);
  return $entities ? reset($entities) : NULL;
}

/**
 * Load multiple delta entries.
 *
 * @param array $dids
 * @param array $conditions
 * @param bool $reset
 * @return mixed
 */
function _dropbox_share_delta_load_multiple($dids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('dropbox_share_delta', $dids, $conditions, $reset);
}

/**
 * Given a delta ID, find a random shared equivalent of that entry.
 *
 * @param $did
 * @param null $dropbox_uid
 * @return mixed|null
 */
function _dropbox_share_delta_load_balanced($did, $dropbox_uid = NULL) {

  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.delta');
  $delta_entity = _dropbox_share_delta_load($did);

  if (
    $delta_entity
    && !$delta_entity->dropbox_is_dir
    && $delta_entity->dropbox_path
    && (!$dropbox_uid || $delta_entity->dropbox_uid == $dropbox_uid)
  ) {

    // We have a valid file delta item.
    module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.user');
    if (
      $delta_entity->dropbox_parent_shared_folder_id
      && variable_get('dropbox_share_media_balance', DROPBOX_SHARE_MEDIA_BALANCE)
      && _dropbox_share_user_count()
    ) {
      // Query to find possible parallel delta entities.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'dropbox_share_delta');

      // Match by every parameter we can, ending with a "like" for the file name.
      $query->propertyCondition('dropbox_modifier_uid', $delta_entity->dropbox_modifier_uid);
      $query->propertyCondition('dropbox_rev', $delta_entity->dropbox_rev);
      $query->propertyCondition('dropbox_revision', $delta_entity->dropbox_revision);
      $query->propertyCondition('dropbox_thumb_exists', $delta_entity->dropbox_thumb_exists);
      $query->propertyCondition('dropbox_icon', $delta_entity->dropbox_icon);
      $query->propertyCondition('dropbox_parent_shared_folder_id', $delta_entity->dropbox_parent_shared_folder_id);
      $query->propertyCondition('dropbox_bytes', $delta_entity->dropbox_bytes);
      $query->propertyCondition('dropbox_client_mtime', $delta_entity->dropbox_client_mtime);
      $query->propertyCondition('dropbox_modified', $delta_entity->dropbox_modified);
      $query->propertyCondition('dropbox_root', $delta_entity->dropbox_root);
      $query->propertyCondition('dropbox_mime_type', $delta_entity->dropbox_mime_type);

      // Finally match with a like against the file name.
      $file_segments = explode('/', $delta_entity->dropbox_path);
      $file_name = end($file_segments);
      $query->propertyCondition('dropbox_path', '%/' . $file_name, 'LIKE');

      $result = $query->execute();
      if (
        !empty($result['dropbox_share_delta'])
        && count($result['dropbox_share_delta']) > 1
      ) {
        // Randomly distribute the shared folder usage across delta items.
        // @todo - In the future load-balance based on known bandwidth stats.
        $did = array_rand($result['dropbox_share_delta']);
        $delta_entity = _dropbox_share_delta_load($did);
      }
    }
  }
  return $delta_entity;
}
