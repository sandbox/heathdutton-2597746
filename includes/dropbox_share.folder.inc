<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Ajax request for a folder listing
 * We should send the result as a nested array with:
 *    title    String. The raw name of the file/folder
 *    key      Int. The delta ID (did) of the entry
 *    folder   Boolean. Optional. Indicating the entry is a folder.
 *    children Array. Optional. The children of this entry.
 *
 * Note: We are intentionally leaving out path/uid data,
 * as we can discern this on media requests.
 *
 * @param int $dropbox_uid
 * @param int $did
 * @param int $children_mode
 */
function dropbox_share_user_folder_ajax($dropbox_uid, $did = 0, $children_mode = 0) {
  $folders = &drupal_static(__METHOD__);
  $cache_key = $dropbox_uid . '/' . $did . '/' . $children_mode;

  // Do not cache this ajax request.
  drupal_page_is_cacheable(FALSE);

  // @todo - Include session-based authentication here to ensure that the current user (by ajax) has access to the folder specified by did.

  if (isset($folders[$cache_key])) {
    $result = $folders[$cache_key];
  }
  elseif (
    $cache = cache_get($cache_key, 'dropbox_share_folder_cache')
    && isset($cache)
    && time() < $cache->expire
  ) {
    $result = $cache->data;
    $folders[$cache_key] = $cache->data;
  }
  else {
    $result = array();

    // By default we want all the root level folders/files.
    $dropbox_path = '/';
    if ($did) {
      // We are retrieving a specific folder...
      module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.delta');
      $parent_delta_entity = _dropbox_share_delta_load($did);

      // Ensure this is an acceptable folder.
      $dropbox_path = NULL;
      if (
        $parent_delta_entity
        && $parent_delta_entity->dropbox_is_dir
        && $parent_delta_entity->dropbox_uid == $dropbox_uid
        && $parent_delta_entity->dropbox_path
      ) {
        $parent = array();
        $path_segments = explode('/', $parent_delta_entity->dropbox_path);
        $parent['title'] = end($path_segments);
        $parent['key'] = intval($parent_delta_entity->id);
        if ($parent_delta_entity->dropbox_is_dir) {
          $parent['folder'] = TRUE;
          $parent['lazy'] = FALSE;
          $parent['children'] = array();
          $dropbox_path = $parent_delta_entity->dropbox_path;
        }
      }
    }

    if ($dropbox_path) {
      $exclude_hidden_files = variable_get('dropbox_share_exclude_hidden_files', DROPBOX_SHARE_EXCLUDE_HIDDEN_FILES);

      // Load the contents of the folder.
      $child_delta_entities = _dropbox_share_folder_contents($dropbox_uid, $dropbox_path);

      // Support nested folder items.
      $children = array();
      foreach ($child_delta_entities as $child_delta_entity) {
        $path_segments = explode('/', $child_delta_entity->dropbox_path);
        $file_name = end($path_segments);
        if ($exclude_hidden_files) {
          if (strpos($file_name, '.') === 0) {
            // This file is hidden and should be skipped.
            continue;
          }
        }
        $child = array();
        $child['title'] = $file_name;
        $child['key'] = intval($child_delta_entity->id);
        if ($child_delta_entity->dropbox_is_dir) {
          $child['folder'] = TRUE;
          $child['lazy'] = TRUE;
        }
        if (isset($child_delta_entity->thumbnail)) {
          $child['thumbnail'] = file_create_url($child_delta_entity->thumbnail->uri);
        }
        $class = _dropbox_share_awesome_class($child_delta_entity->dropbox_icon);
        if ($class) {
          $child['class'] = $class;
        }
        $child['mime_type'] = $child_delta_entity->dropbox_mime_type;

        // Add file size in bytes.
        $child['bytes'] = 0;
        if (isset($child_delta_entity->dropbox_bytes)) {
          $child['bytes'] = intval($child_delta_entity->dropbox_bytes);
        }

        // Define a unique key that can be used for quickly sorting.
        $key = $child['mime_type'] . ' ' . $child['title'];
        while (isset($children[$key])) {
          $key .= '_';
        }
        $children[$key] = $child;
      }

      // Sort children in human readable fashion.
      uksort($children, 'strnatcmp');
      $children = array_values($children);

      // For children mode, we do not need the parent node.
      if ($children_mode) {
        $result = $children;
      }
      else {
        $parent['children'] = $children;
        $result = $parent;
      }
    }

    // Store in our cache for faster retrieval next time.
    cache_set($cache_key, $result, 'dropbox_share_folder_cache', CACHE_TEMPORARY);
    $folders[$cache_key] = $result;
  }

  // Report the entries via json.
  drupal_json_output($result);

  // Since no caching takes place we can skip further all future hooks.
  module_invoke_all('exit');
  exit();
}

/**
 * Given a user and/or path, pull the delta contents of the folder
 *
 * @param null $dropbox_uid
 *    The Dropbox user, who's delta we are reading
 * @param null $path
 *    The path of the folder that we wish to list contents of
 * @param bool $include_files
 * @return mixed
 */
function _dropbox_share_folder_contents($dropbox_uid = NULL, $path = NULL, $include_files = TRUE) {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.delta');

  $delta_entities = array();

  // Current path direct descendants.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'dropbox_share_delta');
  if ($dropbox_uid) {
    $query->propertyCondition('dropbox_uid', $dropbox_uid);
  }
  if (!$include_files) {
    $query->propertyCondition('dropbox_is_dir', 1);
  }
  $regex = '^' . preg_quote(rtrim($path, '/')) . '\/[^\/]+$';
  $query->propertyCondition('dropbox_path', $regex, 'REGEXP');
  $query->propertyOrderBy('dropbox_icon'); // Currently a better fit than mime type.
  $query->propertyOrderBy('dropbox_path');
  $result = $query->execute();
  if (!empty($result['dropbox_share_delta'])) {
    $dids = array_keys($result['dropbox_share_delta']);
    $delta_entities = _dropbox_share_delta_load_multiple($dids);

    // Load thumbnails as well, if enabled
    if (
      $delta_entities
      && $include_files
      && variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS)
    ) {
      module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.thumbnail');
      $thumbnail_entities = _dropbox_share_thumbnail_load_multiple_by_delta(array_keys($delta_entities));
      // Append thumbnail entities to the delta entities prior to output.
      foreach ($thumbnail_entities as $thumbnail_entity) {
        $delta_entities[$thumbnail_entity->did]->thumbnail = $thumbnail_entity;
      }
    }
  }

  return $delta_entities;
}

/**
 * Given a dropbox user, clear the folder cache.
 * You'll want to do this if there is any significant change to delta.
 *
 * @param $dropbox_uid
 */
function _dropbox_share_folder_cache_clear($dropbox_uid) {
  if ($dropbox_uid) {
    cache_clear_all($dropbox_uid . '/', 'dropbox_share_folder_cache', TRUE);
  }
}

/**
 * Map dropbox-standard icons to font-awesome icons.
 *
 * @param  string $dropbox_icon
 * @return string
 */
function _dropbox_share_awesome_class($dropbox_icon) {
  $result = NULL;
  $dropbox_to_awesome = array(
    'page_white_code' => 'fa-code',
    'page_white_text' => 'fa-file-text',
    'page_white_picture' => 'fa-picture-o',
    'page_white_film' => 'fa-video-camera',
  );
  if (isset($dropbox_to_awesome[$dropbox_icon])) {
    $result = 'fa ' . $dropbox_to_awesome[$dropbox_icon];
  }
  return $result;
}
