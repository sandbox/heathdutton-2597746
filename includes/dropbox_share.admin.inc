<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Global configuration for Dropbox Share module.
 *
 * @return mixed
 *  dropbox_share_auth_redirect_uri.
 */
function dropbox_share_admin_form() {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.auth');

  $form = array();

  $form['dropbox_share_app'] = array(
    '#type' => 'select',
    '#title' => t('Dropbox app'),
    '#description' => t("Use the default App which is maintained by this module's contributors, or use your own custom App for improved security and control. Changing the App will not drop previously connected accounts."),
    '#options' => array(
      DROPBOX_SHARE_APP_DEFAULT => t('Default'),
      DROPBOX_SHARE_APP_CUSTOM => t('Custom'),
    ),
    '#attributes' => array(
      'name' => array('dropbox_share_app')
    ),
    '#default_value' => variable_get('dropbox_share_app', DROPBOX_SHARE_APP_DEFAULT),
  );

  $form['custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Dropbox app settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="dropbox_share_app"]' => array(
          'value' => (string) DROPBOX_SHARE_APP_CUSTOM
        ),
      ),
    ),
  );

  $redirect_uri = _dropbox_share_auth_redirect_uri();
  $form['custom']['markup'] = array(
    '#markup' => '<p>' . t(
        'To use a custom app: </br>' .
        '1. Ensure https is enabled for your domain (a valid certificate is not required).<br/>' .
        '2. Go <a href="https://www.dropbox.com/developers/apps/" target="_blank">here</a> to create your app.<br/>' .
        '3. Add "@uri" to your list of "Redirect URIs" under OAuth 2 for your app.<br/>' .
        '4. Insert your own app key and secret below.<br/>'
        , array('@uri' => $redirect_uri)) . '</p>',
  );

  $form['custom']['dropbox_share_app_key'] = array(
    '#type' => 'textfield',
    '#title' => t('App key'),
    '#description' => t('If you wish to authenticate against your own private application, then insert your app key here.'),
    '#default_value' => variable_get('dropbox_share_app_key', DROPBOX_SHARE_APP_KEY),
  );

  $form['custom']['dropbox_share_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#description' => t('If you wish to authenticate against your own private application, then insert your app secret here.'),
    '#default_value' => variable_get('dropbox_share_app_secret', DROPBOX_SHARE_APP_SECRET),
  );

  $form['dropbox_share_delta_storage'] = array(
    '#type' => 'select',
    '#title' => t('Delta storage'),
    '#description' => t('This will maintain a list of files and folders for every connected Dropbox account. This drastically improves the performance of browsing and allow thumbnails and downloads without risk of timeouts. This should only be disabled for unusual circumstances or debugging.'),
    '#options' => array(
      NULL => t('Disabled'),
      DROPBOX_SHARE_DELTA_STORAGE => t('Enabled (Default)'),
    ),
    '#default_value' => variable_get('dropbox_share_delta_storage', DROPBOX_SHARE_DELTA_STORAGE),
    '#attributes' => array(
      'name' => array('dropbox_share_delta_storage'),
    ),
  );

  $form['delta'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delta Settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="dropbox_share_delta_storage"]' => array(
          'value' => DROPBOX_SHARE_DELTA_STORAGE
        ),
      ),
    ),
  );

  $form['delta']['dropbox_share_delta_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Synchronize by cron'),
    '#description' => t('Update file and folder data on each cron run. This includes thumbnails if enabled. You can customize the frequency with Ultimate Cron or Elysia Cron. Default: enabled'),
    '#default_value' => variable_get('dropbox_share_delta_cron', DROPBOX_SHARE_DELTA_CRON),
  );

  $form['delta']['dropbox_share_delta_user_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Synchronize by ajax (permission required)'),
    '#description' => t('Whenever you are logged in and have the permission "synchronize dropbox share delta", the delta will be kept in sync by ajax and you will be notified of synchronizations. Default: enabled'),
    '#default_value' => variable_get('dropbox_share_delta_user_sync', DROPBOX_SHARE_DELTA_USER_SYNC),
  );

  $form['delta']['dropbox_share_exclude_hidden_files'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude hidden files and folders'),
    '#description' => t('Enable this to exclude the listing of files/folders that begin with a period. Default: enabled'),
    '#default_value' => variable_get('dropbox_share_exclude_hidden_files', DROPBOX_SHARE_EXCLUDE_HIDDEN_FILES),
  );

  $form['delta']['dropbox_share_delta_rest'] = array(
    '#type' => 'select',
    '#title' => t('Rest between synchronizations'),
    '#description' => t('Once everything is in sync, we will stop testing for changes for a while on a per-account basis. This will not impact the speed of synchronization, only how often we check for changes after we reach complete synchronization.'),
    '#options' => _dropbox_share_delta_rest_options(),
    '#default_value' => variable_get('dropbox_share_delta_rest', DROPBOX_SHARE_DELTA_REST),
  );

  $form['delta']['dropbox_share_delta_thumbnails'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnails'),
    '#description' => t('Thumbnails will be downloaded when available and stored in Drupal to make browsing more pleasant. Thumbnails are updated progressively along with the synchronizations above. If this setting is changed thumbnails will be updated automatically.'),
    '#options' => _dropbox_share_thumbnail_options(),
    '#default_value' => variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS),
    '#attributes' => array(
      'name' => array('dropbox_share_delta_thumbs')
    ),
  );

  $form['delta']['dropbox_share_thumbnail_rate'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail retrieval rate'),
    '#description' => t('The number of thumbnails we will try to download at a time. Decrease this if you encounter timeouts.'),
    '#options' => _dropbox_share_thumbnail_rate_options(),
    '#default_value' => variable_get('dropbox_share_thumbnail_rate', DROPBOX_SHARE_THUMBNAIL_RATE),
    '#states' => array(
      'invisible' => array(
        ':input[name="dropbox_share_delta_thumbs"]' => array(
          'value' => '',
        ),
      ),
    ),
  );

  $form['delta']['dropbox_share_media'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow downloads'),
    '#description' => t('Allows users to download files directly from Dropbox. If disabled files can be listed/previewed but never downloaded. If enabled we must keep the URLs cached for a time to avoid overloading with API requests.'),
    '#default_value' => variable_get('dropbox_share_media', DROPBOX_SHARE_MEDIA),
    '#attributes' => array(
      'name' => array('dropbox_share_media'),
    ),
  );

  $form['delta']['dropbox_share_media_balance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load-balance downloads when possible'),
    '#description' => t('Automatically load-balance downloads across shared folders when possible. This allows you to get beyond the daily bandwidth limit when possible. Default: enabled'),
    '#default_value' => variable_get('dropbox_share_media_balance', DROPBOX_SHARE_MEDIA_BALANCE),
    '#states' => array(
      'invisible' => array(
        ':input[name="dropbox_share_media"]' => array(
          'value' => '0',
        ),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Begin Dropbox oauth request by redirecting off site to Dropbox
 *
 * @throws \Exception
 * @throws \OAuthException
 */
function dropbox_share_admin_user_add() {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.auth');

  $web_auth = _dropbox_share_auth_start();
  if ($web_auth) {

    if (intval(variable_get('dropbox_share_app', DROPBOX_SHARE_APP_DEFAULT)) == DROPBOX_SHARE_APP_CUSTOM) {
      // Custom apps do not need the intermediary oauth-redirect.
      $url_state = NULL;
    }
    else {
      // The Default app does not need the intermediary oauth-redirect
      // Get the path as if it were a custom app for the second redirection.
      $url_state = 'oauth-redirect=' . _dropbox_share_auth_redirect_uri(TRUE, FALSE);
    }

    $authorize_url = $web_auth->start($url_state);
    if ($authorize_url) {
      drupal_goto($authorize_url, array('external' => TRUE));
    }
  }
}

/**
 * Admin operation clicked to delete a Dropbox User account.
 *
 * @param $dropbox_uid
 */
function dropbox_share_admin_user_delete($dropbox_uid) {

}

/**
 * Handle returning from oauth requests via Dropbox.
 */
function dropbox_share_admin_user_redirect() {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.auth');

  // Complete the auth request in progress.
  $dropbox_user = _dropbox_share_web_auth_complete();

  // Handle redirection, user messages and retries.
  if ($dropbox_user) {
    // Successful handshake. Account linked. Show the account list page.
    drupal_set_message(
      t('The account for @dropbox_display_name has been @action.',
        array(
          '@dropbox_display_name' => $dropbox_user->dropbox_display_name,
          '@action' => (isset($dropbox_user->is_new) && $dropbox_user->is_new) ? t('created') : t('updated'),
        )
      ), 'status', FALSE
    );
    drupal_goto('admin/config/services/dropbox-share');
  }
  else {
    if (!isset($_SESSION['dropbox_share_redirect_retries'])) {
      $_SESSION['dropbox_share_redirect_retries'] = 0;
    }
    if ($_SESSION['dropbox_share_redirect_retries'] < DROPBOX_SHARE_REDIRECT_RETRY_LIMIT) {
      // Redirect and retry authentication.
      $_SESSION['dropbox_share_redirect_retries']++;
      drupal_goto('admin/config/services/dropbox-share/add');
    }
    else {
      drupal_set_message(
        t('The Dropbox User could not be created. View logs for details.'),
        'error', FALSE
      );
      drupal_goto('admin/config/services/dropbox-share');
    }
  }
}

/**
 * Options for delta rest times.
 *
 * @return array
 */
function _dropbox_share_delta_rest_options() {
  $options = array(
    30 => t('Rest for 30s'),
    60 => t('Rest for 1m'),
    300 => t('Rest for 5m'),
    600 => t('Rest for 10m'),
    1800 => t('Rest for 30m'),
    3600 => t('Rest for 1h'),
  );
  foreach ($options as $rate => $label) {
    if ($rate == DROPBOX_SHARE_DELTA_REST) {
      $options[$rate] .= ' ' . t('(Default)');
    }
  }
  return $options;
}

/**
 * Current Dropbox thumbnail options per the API
 *
 * @return array
 */
function _dropbox_share_thumbnail_options() {
  $options = array(
    '' => t('Disabled'),
    'xs' => t('Extra Small - 32x32px'),
    's' => t('Small - 64x64px'),
    'm' => t('Medium - 128x128px'),
    'l' => t('Large - 640x640px'),
    'xl' => t('Extra Large - 1024x1024px'),
  );
  $options[DROPBOX_SHARE_DELTA_THUMBNAILS] .= ' ' . t('(Default)');
  return $options;
}

/**
 * Options for thumbnail cache rates.
 *
 * @return array
 */
function _dropbox_share_thumbnail_rate_options() {
  $options = array();
  foreach (range(10, 100, 10) as $rate) {
    $options[$rate] = t('Cache @rate at a time', array('@rate' => $rate));
    if ($rate == DROPBOX_SHARE_THUMBNAIL_RATE) {
      $options[$rate] .= ' ' . t('(Default)');
    }
  }
  return $options;
}