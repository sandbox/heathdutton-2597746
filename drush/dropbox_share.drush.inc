<?php

/**
 * @file
 *   drush integration for dropbox_share.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @return array
 *   An associative array describing the command(s).
 */
function dropbox_share_drush_command() {
  $items = array();

  $items['dropbox-share-sync'] = array(
    'callback' => 'dropbox_share_drush_sync',
    'description' => dt('Runs a synchronization background task unit'),
    'aliases' => array('dbss'),
    'arguments' => array(
      'stats' => dt('Optional. Output the statistics of the request.'),
      'dropbox_uid' => dt('Optional. Specify a Dropbox UID to synchronize a specific account.'),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for the command.
 */
function dropbox_share_drush_help($section) {
  switch ($section) {
    case 'drush:dropbox-share-sync':
      return dt('Runs a synchronization background task unit');
  }
}

/**
 * Run a synchronization background task unit.
 */
function dropbox_share_drush_sync() {
  module_load_include('inc', 'dropbox_share', 'includes/dropbox_share.sync');

  $dropbox_uid = drush_get_option('dropbox_uid', NULL);

  $stats = _dropbox_share_sync(intval($dropbox_uid));

  if (drush_get_option('stats', NULL)) {
    drush_print(dt('Synchronization completed. Statistics: '));
    drush_print_r($stats);
  }
  else {
    drush_print(dt('Synchronization completed.'));
  }
}
