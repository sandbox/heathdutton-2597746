; Drupal version
core = 7.x
api = 2

; Community dependencies for Dropbox Share

; Encrypt is used to obfuscate keys, secrets and tokens
projects[encrypt][version] = 2.1
projects[encrypt][subdir] = contrib

; Libraries is needed to depend on external libraries such as the Dropbox SDK
projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib

; jQuery 1.7+ is required for the file browser we use
projects[jquery_update][version] = 3.0-alpha3
projects[jquery_update][subdir] = contrib

; Colorbox is used for gracefully viewing images and video
projects[colorbox][version] = 2.10
projects[colorbox][subdir] = contrib

libraries[colorbox][download][type] = file
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.6.3.tar.gz

; Official Dropbox SDK for PHP
libraries[dropbox-sdk-php][download][type] = file
libraries[dropbox-sdk-php][download][url] = https://github.com/dropbox/dropbox-sdk-php/archive/v1.1.6.tar.gz

; Fontawesome module and adjoining library for folder and file icons
projects[fontawesome][version] = 2.5
projects[fontawesome][subdir] = contrib

libraries[fontawesome][download][type] = file
libraries[fontawesome][download][url] = https://github.com/FortAwesome/Font-Awesome/archive/v4.5.0.tar.gz

libraries[videojs][download][type] = file
libraries[videojs][download][url] = https://github.com/videojs/video.js/archive/v5.8.5.tar.gz
