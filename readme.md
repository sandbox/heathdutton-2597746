Dropbox Share
-------------

Whitelabel Dropbox for mass distribution.

Dropbox is one of the easiest and most cost effective solutions for sharing files with consumers, but there are some important limitations.
This module aims to allow us to overcome those limitations, making Dropbox a seamless part of our file sharing in Drupal.

# Features

In order of priority...

* Connect one (or more) Dropbox accounts by oAuth 2.
  * Allows one to overcome the Dropbox bandwidth cap by load balancing a shared folder across multiple accounts.
* Whitelabel the Dropbox interface entirely within Drupal.
  * Font awesome is used, and styles can easily be overridden to match your theme.
* A field type is provided that can be used to display a folder.
  * Just add a field to your node type, and easily share a Dropbox folder when viewing that node. This is the recommended way of using this module.
* A block is provided that can be used to display a folder.
  * If you use blocks, you can drop one anywhere and configure it to display a specific Dropbox folder.
* A panel pane is provided that can be used to display a folder.
  * If you use panels, you can drop one anywhere and configure it to display a specific Dropbox folder.
* Cache folder structure and thumbnails.
  * This drastically speeds up the opening and displaying of folders and makes the user interface more pleasant to view.
* Allow end-users uploads. 
  * Optionally allow users to upload into any folder, or a separate folder for internal review.
  * You might set up a hidden "review" folder that anyone can upload to, and you can then simply drag the contents into a public folder when/if you deem them worthy.
* Optional Google analytics file/folder tracking.
* Optional comments and voting on a file/folder basis.

# Requirements

If you are running Drupal 7, you are most likely already in compliance.

* PHP 5.3+, with 64-bit integers.
* PHP cURL extension with SSL enabled (it's usually built-in).
* Must not be using mbstring.func_overload to overload PHP's standard string functions.

# Installation

A make file is included for standard 'drush make' installation.
To install manually:

1. Create a folder in your /libraries folder called "dropbox"
2. Extract the [Dropbox PHP Library](https://github.com/dropbox/dropbox-sdk-php/archive/v1.1.6.zip) into your /libraries/dropbox folder.
3. Install this module. If prompted, install the dependency [Libraries](https://www.drupal.org/project/libraries).

# Future development

* To speed up folder listing, progressively fill in a parent_did column, and use that (when delta is up to date) to more quickly find children of a folder without the use of regex.
* Cache the first x nodes of folder structure per field instance for faster initial loading.
* Ensure we have the required version of jQuery enabled
* Create a file manager for choosing files to view/download using https://github.com/browserstate/history.js/
* Add a more convenient way to share a folder with all tied accounts (auto-accept the share).
* Add an app callback at some domain, that allows any Drupal site to auth against the default app.
* Add a App status checker, that ensures https works (by http request), that the key and secret have been entered, and that the oauth URI is configured.
* Accept a dropbox webhook, to invalidate folder hierarchy cache exactly when needed.
* Allow sharing of a single file, instead of a folder.
* Allow "add to my Dropbox" as an option for all files/folders.
* Hooks to implement your own authentication (This can already be done on a block/node view basis, but extension is a possibility).
* Media module integration.
* When Dropbox API v2 is released with the PHP SDK, consider upgrade feasibility.