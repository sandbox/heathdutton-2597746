<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Initialize the Dropbox SDK for new / existing users.
 *
 * @param int $dropbox_user
 * @return \Dropbox\AppInfo|null
 * @throws \Dropbox\AppInfoLoadException
 */
function dropbox_share_api_load($dropbox_user = NULL) {
  $app_info = NULL;
  $library_path = libraries_get_path('dropbox-sdk-php');
  if ($library_path && file_exists($library_path . '/lib/Dropbox/autoload.php')) {
    require_once $library_path . '/lib/Dropbox/autoload.php';

    if (intval(variable_get('dropbox_share_app', DROPBOX_SHARE_APP_DEFAULT)) == DROPBOX_SHARE_APP_CUSTOM) {
      // Custom app behavior.
      $key = variable_get('dropbox_share_app_key', FALSE);
      $key = $key ? decrypt(base64_decode($key)) : DROPBOX_SHARE_APP_KEY;
      $secret = variable_get('dropbox_share_app_secret', FALSE);
      $secret = $secret ? decrypt(base64_decode($secret)) : DROPBOX_SHARE_APP_SECRET;
    }
    elseif ($dropbox_user) {
      // Use the secret and key originally used for this user.
      $key = decrypt(base64_decode($dropbox_user->dropbox_key));
      $secret = decrypt(base64_decode($dropbox_user->dropbox_secret));
    }
    else {
      // Default app behavior.
      $key = DROPBOX_SHARE_APP_KEY;
      $secret = DROPBOX_SHARE_APP_SECRET;
    }

    // We must ensure the same dropbox share key and secret are used throughout
    // the rest of authentication sessions.
    $_SESSION['dropbox_share_key'] = $key;
    $_SESSION['dropbox_share_secret'] = $secret;

    // Load AppInfo.
    $app_info = Dropbox\AppInfo::loadFromJson(array(
      'key' => $key,
      'secret' => $secret,
    ));

  }
  else {

    drupal_set_message(t('The dropbox-sdk-php library could not be loaded. Please make sure it has been installed in your libraries folder.'), 'error', FALSE);
    watchdog(WATCHDOG_ERROR, 'The dropbox-sdk-php library could not be loaded.');

  }
  return $app_info;
}


/**
 * Establish a Client for a particular dropbox user.
 *
 * @param $dropbox_user
 * @return \Dropbox\Client|null\
 */
function dropbox_share_api_client($dropbox_user) {
  $client = NULL;

  $app_info = dropbox_share_api_load($dropbox_user);
  if ($app_info) {
    $client = new Dropbox\Client(decrypt(base64_decode($dropbox_user->dropbox_token)), DROPBOX_SHARE_APP_ID);
  }

  return $client;
}

