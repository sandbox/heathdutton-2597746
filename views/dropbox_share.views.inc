<?php

/**
 * @file
 * Dropbox Share module.
 */

/**
 * Implements hook_views_data().
 */
function dropbox_share_views_data_alter(&$data) {
  $data['dropbox_share_user']['dropbox_team_name'] = array(
    'field' => array(
      'title' => t('Team'),
      'help' => t('The name of the Dropbox team this user is in.'),
      'handler' => 'dropbox_share_handler_dropbox_team_name_field',
    ),
  );
  $data['dropbox_share_user']['dropbox_display_name'] = array(
    'field' => array(
      'title' => t('Name'),
      'help' => t('The name of the user as seen from Dropbox.'),
      'handler' => 'dropbox_share_handler_dropbox_display_name_field',
    ),
  );
  $data['dropbox_share_user']['dropbox_email'] = array(
    'field' => array(
      'title' => t('Email'),
      'help' => t('The email of this user in Dropbox.'),
      'handler' => 'dropbox_share_handler_dropbox_email_field',
    ),
  );
  $data['dropbox_share_user']['status'] = array(
    'field' => array(
      'title' => t('Status'),
      'help' => t('Indicates if this account is enabled or not'),
      'handler' => 'dropbox_share_handler_status_field',
    ),
  );
  $data['dropbox_share_user']['dropbox_delta_has_more'] = array(
    'field' => array(
      'title' => t('Synchronization'),
      'help' => t('Indicates the account synchronization status'),
      'handler' => 'dropbox_share_handler_dropbox_delta_has_more_field',
    ),
  );
  $data['dropbox_share_user']['operations'] = array(
    'field' => array(
      'title' => t('Operations'),
      'help' => t('Remove/Refresh this user from the system.'),
      'handler' => 'dropbox_share_handler_operations_field',
    ),
  );
}

/**
 * Implements hook_views_default_views().
 */
function dropbox_share_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'dropbox_share_user';
  $view->description = 'Lists the connected Dropbox user accounts';
  $view->tag = 'Services';
  $view->base_table = 'dropbox_share_user';
  $view->human_name = 'Dropbox Share - Users';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Dropbox Share';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer dropbox share settings';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'dropbox_team_name' => 'dropbox_team_name',
    'dropbox_display_name' => 'dropbox_display_name',
    'dropbox_email' => 'dropbox_email',
    'status' => 'status',
    'dropbox_delta_has_more' => 'dropbox_delta_has_more',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'dropbox_team_name' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dropbox_display_name' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dropbox_email' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dropbox_delta_has_more' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations' => array(
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: : Team */
  $handler->display->display_options['fields']['dropbox_team_name']['id'] = 'dropbox_team_name';
  $handler->display->display_options['fields']['dropbox_team_name']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['dropbox_team_name']['field'] = 'dropbox_team_name';
  /* Field: : Name */
  $handler->display->display_options['fields']['dropbox_display_name']['id'] = 'dropbox_display_name';
  $handler->display->display_options['fields']['dropbox_display_name']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['dropbox_display_name']['field'] = 'dropbox_display_name';
  /* Field: : Email */
  $handler->display->display_options['fields']['dropbox_email']['id'] = 'dropbox_email';
  $handler->display->display_options['fields']['dropbox_email']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['dropbox_email']['field'] = 'dropbox_email';
  /* Field: : Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: : Synchronization */
  $handler->display->display_options['fields']['dropbox_delta_has_more']['id'] = 'dropbox_delta_has_more';
  $handler->display->display_options['fields']['dropbox_delta_has_more']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['dropbox_delta_has_more']['field'] = 'dropbox_delta_has_more';
  /* Field: : Operations */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'dropbox_share_user';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'dropbox_share_user_list');
  $handler->display->display_options['path'] = 'admin/config/services/dropbox-share/users';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'Users';
  $handler->display->display_options['menu']['description'] = 'View a list of the Dropbox user accounts connected.';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Dropbox Share';
  $handler->display->display_options['tab_options']['description'] = 'View a list of Users';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['dropbox_share_user'] = array(
    t('Master'),
    t('Dropbox Share'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Team'),
    t('Name'),
    t('Email'),
    t('Status'),
    t('Synchronization'),
    t('Operations'),
    t('Page'),
  );

  $views[$view->name] = $view;

  return $views;
}

class dropbox_share_handler_dropbox_display_name_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['dropbox_display_name'] = 'dropbox_display_name';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->get_value($values, 'dropbox_display_name');
  }
}

class dropbox_share_handler_dropbox_team_name_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['dropbox_team_name'] = 'dropbox_team_name';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->get_value($values, 'dropbox_team_name');
  }
}

class dropbox_share_handler_dropbox_email_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['dropbox_email'] = 'dropbox_email';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->get_value($values, 'dropbox_email');
  }
}

class dropbox_share_handler_status_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['status'] = 'status';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->get_value($values, 'status') ? t('Enabled') : t('Disabled');
  }
}

class dropbox_share_handler_dropbox_delta_has_more_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['dropbox_delta_has_more'] = 'dropbox_delta_has_more';
    $this->additional_fields['dropbox_uid'] = 'dropbox_uid';
    $this->additional_fields['updated'] = 'updated';
    $this->additional_fields['thumbnail_percentage'] = 'thumbnail_percentage';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $has_more = $this->get_value($values, 'dropbox_delta_has_more');
    $dropbox_uid = $this->get_value($values, 'dropbox_uid');
    $updated = $this->get_value($values, 'updated');
    $thumbnail_percentage = $this->get_value($values, 'thumbnail_percentage');

    $throbber = '<div class="ajax-progress"><div class="throbber"></div></div>';
    $output = '<span class="dropbox-user-sync" data-dropbox-uid="' . $dropbox_uid .'">';
    $thumbs_enabled = variable_get('dropbox_share_delta_thumbnails', DROPBOX_SHARE_DELTA_THUMBNAILS);

    if ($updated){
      if ($has_more == '1'){
        $output .= t('Syncing item list') . $throbber;
      } else {
        if (
          $thumbs_enabled
          && $thumbnail_percentage < 100
        ){
          $output .= t('Syncing thumbnails: @percentage%', array('@percentage' => $thumbnail_percentage)) . $throbber;
        } else {
          $output .= t('Complete');
        }
      }
    } else if ($has_more == '1'){
      $output .= t('Pending');
    }

    $output .= '</span>';

    return $output;
  }
}

class dropbox_share_handler_operations_field extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['dropbox_uid'] = 'dropbox_uid';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $dropbox_uid = $values->{$this->aliases['dropbox_uid']};

    $admin_base = array(
      'access arguments' => array('administer dropbox share settings'),
      'file' => 'includes/dropbox_share.admin.inc',
    );

    $links = array();

    // @todo - fix this link to delete an account
    $links['admin/config/services/dropbox-share/' . $dropbox_uid . '/delete'] = array(
        'title' => t('delete'),
        'page arguments' => array('dropbox_share_user_delete'),
        'type' => MENU_CALLBACK,
      ) + $admin_base;

    // @todo - add disable & enable options

    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
