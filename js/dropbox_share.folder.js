/**
 * @file
 * Dropbox Share - Folder widget
 *
 * Behaviors for widget in use by content editors to select files/folders
 * to be shared with end users
 */
(function ($) {
  Drupal.behaviors.DropboxShareFolder = {
    attach: function (context, settings) {

      if (typeof jQuery.fn.fancytree != 'undefined') {

        function format_bytes(bytes, decimals) {
          if (bytes == 0) return 'empty';
          var k = 1000; // or 1024 for binary
          var dm = decimals + 1 || 3;
          var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
          var i = Math.floor(Math.log(bytes) / Math.log(k));
          return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        }

        $(context).find('input.dropbox-share-did').each(function () {

          var did_input = $(this),
              did_label = did_input.parent().parent().find('label[for=did]'),
              did = did_input.val(),
              uid_input = did_input.parent().parent().find('.dropbox-share-uid'),
              uid = uid_input.val(),
              download = settings.dropbox_share.media.download,
              container = did_input.parent()
                  .append('<div class="dropbox-share-did"></div>')
                  .find('div.dropbox-share-did').first();

          if (!did && typeof settings.dropbox_share.folder.did != 'undefined') {
            did = settings.dropbox_share.folder.did;
            did_input.val(did);
          }
          if (!uid && typeof settings.dropbox_share.folder.uid != 'undefined') {
            uid = settings.dropbox_share.folder.uid;
            uid_input.val(uid);
          }
          did_input.hide();

          // Allow an admin to easily get back to the root path.
          if (typeof settings.dropbox_share.folder.allow_traversal != 'undefined' && settings.dropbox_share.folder.allow_traversal) {
            container.prepend('<div class="dropbox-share-did-root"><i class="fa fa-caret-left"></i>' + Drupal.t('Go back to root') + '</div>')
                .find('.dropbox-share-did-root')
                .click(function(){
                  did_input.val('');
                  uid_input.trigger('change');
                });
          }

          // Bind event to restart fancytree if new uid is selected
          uid_input.change(function(){
            var uid = uid_input.val();
            if (uid != "0"){
              container.show();
              did_label.show();
              container.fancytree({
                extensions: [
                  'persist',
                  'glyph',
                  'contextMenu'
                ],
                source: {
                  url: settings.dropbox_share.folder.ajax_url + '/' + uid + '/' + did_input.val(),
                  cache: true
                },
                lazyLoad: function(event, data) {
                  data.result = {
                    url: settings.dropbox_share.folder.ajax_url + '/' + uid + '/' + data.node.key + '/1',
                    cache: true
                  };
                },
                persist: {
                  expandLazy: true,
                  // fireActivate: false, // false: suppress `activate` event after active node was restored
                  // overrideSource: false, // true: cookie takes precedence over `source` data attributes.
                  store: "auto" // 'cookie', 'local': use localStore, 'session': sessionStore
                },
                activeVisible: true, // Make sure, active nodes are visible (expanded).
                aria: true, // Enable WAI-ARIA support.
                autoActivate: true, // Automatically activate a node when it is focused (using keys).
                autoCollapse: true, // Automatically collapse all siblings, when a node is expanded.
                autoScroll: true, // Automatically scroll nodes into visible area.
                clickFolderMode: 3, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
                checkbox: false, // Show checkboxes.
                debugLevel: 1, // 0:quiet, 1:normal, 2:debug
                disabled: false, // Disable control
                focusOnSelect: true, // Set focus when node is checked by a mouse click
                generateIds: true, // Generate id attributes like <span id='fancytree-id-KEY'>
                idPrefix: "did-", // Used to generate node id´s like <span id='fancytree-id-<key>'>.
                keyboard: true, // Support keyboard navigation.
                keyPathSeparator: "/", // Used by node.getKeyPath() and tree.loadKeyPath().
                minExpandLevel: 1, // 1: root node is not collapsible
                quicksearch: true, // Navigate to next node by typing the first letters.
                selectMode: 1, // 1:single, 2:multi, 3:multi-hier
                tabbable: true, // Whole tree behaves as one single control
                titlesTabbable: true, // Node titles can receive keyboard focus
                // Glyph map for font-awesome defaults (overridden by dropbox icon map in dropbox_share.folder.inc)
                glyph: {
                  map: {
                    doc: 'fa fa-file-text',
                    docOpen: 'fa fa-file-text-o',
                    checkbox: 'fa fa-square',
                    checkboxSelected: 'fa fa-check-square',
                    checkboxUnknown: 'fa fa-square',
                    dragHelper: 'fa arrow-right',
                    dropMarker: 'fa long-arrow-right',
                    error: 'fa fa-warning',
                    expanderClosed: 'fa fa-caret-right',
                    expanderLazy: 'fa fa-caret-right', // Ajax not yet retrieved
                    expanderOpen: 'fa fa-caret-down',
                    folder: 'fa fa-folder',
                    folderOpen: 'fa fa-folder-open',
                    loading: 'fa fa-refresh fa-spin'
                  }
                },
                contextMenu: {
                  menu: function(node, action, options) {
                    var menu = {};
                    if (node.folder) {
                      // This is a folder, the option to download the whole thing should be provided.
                      if (node.expanded) {
                        $.extend(menu, { 'close-folder': { 'name': Drupal.t('Close'), 'icon': 'paste' }});
                      } else {
                        $.extend(menu, { 'open-folder': { 'name': Drupal.t('Open'), 'icon': 'paste' }});
                      }
                      $.extend(menu, { 'download-folder': { 'name': Drupal.t('Download this folder (zip up to 2GB)'), 'icon': 'paste' }});
                    } else {
                      // This is a single file.
                      $.extend(menu, {'preview': { 'name': Drupal.t('Preview') }});
                      $.extend(menu, {'download-file': { 'name': Drupal.t('Download (@size)', {'@size': format_bytes(node.data.bytes)}), 'icon': 'paste'}});
                    }
                    return menu;
                  },
                  actions: function(node, action, options) {
                    switch(action){
                      case 'open-folder':
                          if (!node.expanded) {
                            jQuery(node.li).trigger('click');
                          }
                        break;

                      case 'close-folder':
                          if (node.expanded) {
                            jQuery(node.li).trigger('click');
                          }
                        break;

                      case 'preview':
                          jQuery(node.li).trigger('click');
                        break;

                      case 'download-file':
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + node.key + '/download';
                          container.append('<iframe src="' + url + '" style="width: 0px; height: 0px; display: none;"></iframe>');
                        break;

                      case 'download-folder':
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + node.key + '/download';
                          container.append('<iframe src="' + url + '" style="width: 0px; height: 0px; display: none;"></iframe>');
                        break;
                    }
                  }
                },
                icon: function (event, data) {
                  if (typeof data.node.data.class != 'undefined'){
                    return data.node.data.class;
                  }
                },
                // Add thumbnails if found
                renderNode: function (event, data) {
                  var span = $(data.node.span);

                  // @todo - Make gallery next/prev buttons work.

                  // Display thumbnails if provided
                  if (typeof data.node.data.thumbnail != 'undefined') {
                    $(data.node.li).attr('data', 'dropbox-share-thumb-wrapper');
                    var title = span.find('.fancytree-title');
                    title.html('<img src="' + data.node.data.thumbnail + '" class="dropbox-share-thumb"\/><span class="inner-title">' + title.text() + '</span>');
                  }
                },
                // User clicked a file, if downloading is enabled allow an ajax
                // request to retrieve a temporary (load balanced) url to the file.
                click: function(event, data) {
                  // Set the input value to save the selection
                  did_input.val(data.node.key);

                  // Log Google Analytics page/file view.
                  if (typeof window.ga == 'function'){
                    var n = data.node,
                        path = n.title;
                    while (
                      typeof n.parent != 'undefined'
                      && n.parent
                      && typeof n.parent.title != 'undefined'
                      && n.parent.title
                      && n.parent.title != 'root'
                    ){
                      n = n.parent;
                      path = n.title + '/' + path;
                    }
                    path = window.location.href + '/' + path;
                    ga('send', 'pageview', path);
                  }

                  if (download && !data.node.folder){
                    // A direct download url. A redirect will proceed to the appropriate Dropbox endpoint.
                    switch (data.node.data.mime_type){
                      case 'image/jpeg':
                      case 'image/gif':
                      case 'image/png':
                      case 'image/bmp':
                        // Colorbox image.
                        if (typeof $.colorbox != 'undefined'){
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/download';
                          var html = '<img src="' + url + '" style="width: 100%; height: auto;" class="dropbox-share-img"/>';
                          $.colorbox({
                            title: function () {
                              return data.node.title + ' <a href="' + url + '" download="' + data.node.title.replace(/(<([^>]+)>)/ig, "") + '" class="dropbox-share-download-link"><i class="fa fa-download"></i>' + Drupal.t('Download') + '</a>';
                            },
                            className: 'dropbox-share-colorbox',
                            speed: 0,
                            html: html,
                            onComplete: function(){
                              // Re-adjust the size of the colorbox once the image has loaded
                              $(document).ready(function(){
                                $('.dropbox-share-img').load(function(){
                                  $.colorbox.resize();
                                });
                              });
                            },
                            innerWidth: '90%',
                            innerHeight: '90%',
                            rel: 'dropbox-share-img'
                          });
                        } else {
                          // Direct download by iframe.
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/download';
                          container.append('<iframe src="' + url + '" style="width: 0px; height: 0px; display: none;"></iframe>');
                        }
                        break;
                      case 'video/3gpp':
                      case 'video/3gpp2':
                      case 'video/h261':
                      case 'video/h263':
                      case 'video/h264':
                      case 'video/jpeg':
                      case 'video/jpm':
                      case 'video/mj2':
                      case 'video/mp4':
                      case 'video/mpeg':
                      case 'video/ogg':
                      case 'video/quicktime':
                      case 'video/webm':
                      case 'video/x-f4v':
                      case 'video/x-fli':
                      case 'video/x-flv':
                      case 'video/x-m4v':
                        // Colorbox video with videojs
                        if (typeof $.colorbox != 'undefined' && typeof window.videojs != 'undefined') {

                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/download',
                            video_id = 'dropbox-share-video',
                            timer = null;
                            html =
                              '<video style="min-height:450px" autoplay id="' + video_id + '" class="dropbox-share-video video-js vjs-default-skin vjs-big-play-centered vjs-playing" controls preload="auto">' +
                              '   <source src="' + url + '" type="' + data.node.data.mime_type + '">' +
                              '   <p class="vjs-no-js">' +
                              '   ' + Drupal.t('Your browser does not support the video tag. You may need to download this video instead.') +
                              '   </p>' +
                              '</video>';
                          $.colorbox({
                            open: true,
                            title: function () {
                              return data.node.title + ' <a href="' + url + '" download="' + data.node.title.replace(/(<([^>]+)>)/ig, "") + '" class="dropbox-share-download-link"><i class="fa fa-download"></i>' + Drupal.t('Download') + '</a>';
                            },
                            innerWidth: '80%',
                            innerHeight: '80%',
                            rel: video_id,
                            maxHeight: '100%',
                            maxWidth: '100%',
                            preloading: false,
                            iframe: false,
                            inline: false,
                            photo: false,
                            className: 'dropbox-share-colorbox',
                            speed: 0,
                            html: '<div id="dropbox-share-video-container"></div>',
                            onComplete: function () {
                              $(context)
                                .find('#dropbox-share-video-container')
                                .html(html)
                                .find('#' + video_id)
                                .first()
                                .each(function () {
                                  videojs(video_id).ready(function() {
                                    $.colorbox.resize();
                                    clearInterval(timer);
                                    timer = setInterval(function(){
                                      $.colorbox.resize();
                                    }, 500);
                                  });
                                }).parent()
                                  .parent()
                                  .find('#cboxTitle')
                                  .addClass('dropbox-share-video-title');
                              $(context).find('#cboxOverlay').unbind('mouseover');
                            },
                            onCleanup: function () {
                              // On close, destroy video so that they stop playing
                              videojs(video_id).dispose();
                            }
                          });
                        } else if (typeof $.colorbox != 'undefined') {
                          // Colorbox video (iframe)
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/download';
                          var html = '<video controls="" autoplay="" name="media" style="min-width: 100%; min-height: 100%;" class="dropbox-share-video"><source src="' + url + '" type="' + data.node.data.mime_type + '">' + Drupal.t('Your browser does not support the video tag.') + '</video>';
                          $.colorbox({
                            open: true,
                            title: function () {
                              return data.node.title + ' <a href="' + url + '" download="' + data.node.title.replace(/(<([^>]+)>)/ig, "") + '" class="dropbox-share-download-link"><i class="fa fa-download"></i>' + Drupal.t('Download') + '</a>';
                            },
                            className: 'dropbox-share-colorbox',
                            speed: 0,
                            html: '<div class="dropbox-share-video-placeholder"></div>',
                            onComplete: function () {
                              $(document).ready(function () {
                                $('.dropbox-share-video-placeholder').html(html)
                                    .removeClass('dropbox-share-video-placeholder');
                                $.colorbox.resize();
                              });
                            },
                            innerWidth: '80%',
                            innerHeight: '80%',
                            rel: 'dropbox-share-video'
                          });
                        } else {
                          // Preview/play by page redirect
                          var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/preview';
                          window.location.href = url;
                        }
                        break;
                      default:
                        // Download download by iframe
                        var url = settings.dropbox_share.media.ajax_url + '/' + uid + '/' + data.node.key + '/download';
                        container.append('<iframe src="' + url + '" style="width: 0px; height: 0px; display: none;"></iframe>');
                    }
                  }
                }
              });
            } else {
              container.hide();
              did_label.hide();
            }

          }).trigger('change');
        });
      }
    }
  };
})(jQuery);
