/**
 * @file
 * Dropbox Share - Sync
 *
 * Behaviours for use by admins using the Dropbox Share module with
 * synchronization enabled when logged in.
 */
(function ($) {
  Drupal.behaviors.DropboxShareSync = {
    attach: function (context, settings) {
      if (typeof settings.dropbox_share != 'undefined'
        && typeof window.dropbox_sync_initiated == 'undefined') {
        window.dropbox_sync_initiated = true;
        $(document).ready(function(){

          // Function for popup notifications
          function notify(string, delay) {
            if (typeof toastr != 'undefined') {
              toastr.options = {
                'closeButton': false,
                'debug': false,
                'newestOnTop': false,
                'progressBar': true,
                'positionClass': 'toast-bottom-right',
                'preventDuplicates': false,
                'onclick': null,
                'showDuration': '50',
                'hideDuration': '200',
                'timeOut': settings.dropbox_share.delta.sync_poll * 2,
                'extendedTimeOut': settings.dropbox_share.delta.sync_poll * 2,
                'showEasing': 'swing',
                'hideEasing': 'linear',
                'showMethod': 'fadeIn',
                'hideMethod': 'fadeOut'
              };
              var name = Drupal.t('Dropbox');
              if (typeof delay != 'undefined' && delay) {
                setTimeout(function () {
                  toastr.info(string, name);
                }, delay);
              } else {
                toastr.info(string, name);
              }
              if (typeof console.log != 'undefined') {
                console.log('Dropbox: ' + string);
              }
            }
          }

          // Number of ms to delay till the next segment of the hour based on
          // the polling interval. Allows notifications to appear to be more
          // real-time across devices, even though we are polling at random start
          // times to prevent server strain.
          function next_segment_delay() {
            var d = new Date(),
                m_ms = d.getMinutes() * 60000,
                s_ms = d.getSeconds() * 1000,
                ms = d.getMilliseconds(),
                ms_of_the_hr = m_ms + s_ms + ms,
                rate_ms = settings.dropbox_share.delta.sync_poll * 1.1,
                next_segment = Math.ceil(ms_of_the_hr / rate_ms),
                nest_segment_ms = (next_segment * rate_ms) - ms_of_the_hr;
            return nest_segment_ms > 0 ? nest_segment_ms : 0;
          }

          // Synchronize delta/thumbnail data
          var last_called = $.cookie('dropbox_share_last_called');
          if (typeof last_called == 'undefined') {
            last_called = 0;
            $.cookie('dropbox_share_last_called', last_called, {
              expires: 7,
              path: '/'
            });
          }

          function sync(dropbox_uid) {
            if (window.dropbox_sync_started === false){
              window.dropbox_sync_started = true;
              $.ajax({
                url: settings.dropbox_share.delta.ajax_url + (typeof dropbox_uid != 'undefined' ? '/' + dropbox_uid : ''),
                dataType: 'json',
                success: function (data) {
                  var wait_ms = settings.dropbox_share.delta.sync_poll;

                  if (
                      typeof data != 'undefined'
                      && typeof data.stats != 'undefined'
                  ) {
                    if (
                        typeof data.stats.started != 'undefined'
                        && data.stats.started > last_called
                    ) {
                      last_called = data.stats.started;
                      $.cookie('dropbox_share_last_called', last_called, {
                        expires: 7,
                        path: '/'
                      });
                      // Display notifications if any by way of a queue
                      if (typeof data.notifications != 'undefined'
                          && data.notifications.length) {
                        var next_segment_ms = next_segment_delay();
                        for (var i = 0; i < data.notifications.length; ++i) {
                          notify(data.notifications[i], next_segment_ms);
                          // notify(i + ' ' + last_called + ' ' + data.notifications[i], next_segment_ms);
                          next_segment_ms += settings.dropbox_share.delta.sync_poll / data.notifications.length;
                        }
                      }
                    }
                  } else if (typeof console.error != 'undefined') {
                    console.error(data);
                  }

                  // Wait till the next retrieval poll
                  setTimeout(function () {
                    window.dropbox_sync_started = false;
                    sync();
                  }, wait_ms);
                },
                error: function (data) {
                  setTimeout(function () {
                    window.dropbox_sync_started = false;
                    sync();
                  }, settings.dropbox_share.delta.sync_poll * 100);
                }
              });
            }
          }

          // Standard ajax based synchronization, if the user has permission to do so
          // call asynchronously to prevent DOM blocking.
          if (settings.dropbox_share.delta.user_sync) {
            setTimeout(function () {
              window.dropbox_sync_started = false;
              sync();
            }, settings.dropbox_share.delta.sync_poll);
          }
        });
      }
    }
  };
})(jQuery);
